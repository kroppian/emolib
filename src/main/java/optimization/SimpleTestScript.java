/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package optimization;

;
import distancemetrics.DistanceMetric;
import distancemetrics.PerpendicularDistanceMetric;
import emo.DoubleAssignmentException;
import emo.Individual;
import emo.OptimizationProblem;
import engines.AbstractGeneticEngine;
import engines.NSGA3Engine;
import engines.UnifiedNsga3Engine;
import evaluators.GeneralDTLZ1Evaluator;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.xml.stream.XMLStreamException;
import parsing.IndividualEvaluator;
import parsing.InvalidOptimizationProblemException;
import parsing.StaXParser;
import utils.RandomNumberGenerator;

/**
 *
 * @author mabouali
 */
public class SimpleTestScript extends TestScript {

    public static void main(String[] args) throws XMLStreamException, InvalidOptimizationProblemException,  DoubleAssignmentException, IOException {

        File nsgaConfig = new File("src/main/resources/samples/dtlz1_7var_3obj.xml");
        String nsgaConfigFilename = nsgaConfig.getAbsoluteFile().toString();
        URL url = new File(nsgaConfigFilename).toURI().toURL();

        InputStream in = url.openStream();
        OptimizationProblem optimizationProblem = StaXParser.readProblem(in);
        // Create Evaluator
        IndividualEvaluator individualEvaluator = new GeneralDTLZ1Evaluator(optimizationProblem);
        double seed = 0.3;
        RandomNumberGenerator.setSeed(seed);
        DistanceMetric met = new PerpendicularDistanceMetric();

        AbstractGeneticEngine geneticEngine = new UnifiedNsga3Engine(optimizationProblem, individualEvaluator, met);
        File outputDir = new File(topOutDir + File.separator + String.format("%s-%03d-%03d-P%04d-G%04d/%s/",
                optimizationProblem.getProblemID(),
                optimizationProblem.getRealCrossoverDistIndex(),
                optimizationProblem.getRealMutationDistIndex(),
                optimizationProblem.getPopulationSize(),
                optimizationProblem.getGenerationsCount(),
                geneticEngine.getAlgorithmName()));
        // Make directories
        if (!outputDir.exists()) {
            outputDir.mkdirs();
        }

        int totalEvaluationsCount = 0;
        int runsCount = 5;
        for (int runIndex = 0; runIndex < runsCount; runIndex++) {
            System.out.println("*****************");
            System.out.format("    Run(%03d)  %n", runIndex);
            System.out.println("*****************");
            // Create output directory of this specific run
            File runOutputDir = new File(outputDir.getPath() + File.separator + String.format("run%03d/", runIndex));
            if (!runOutputDir.exists()) {
                runOutputDir.mkdir();
            }
            // Start the engine
            //Individual[] finalPopulation = geneticEngine.start(outputDir, runIndex, epsilon, asfMinimizer, kktCalculator);
            Individual[] finalPopulation = geneticEngine.start(runOutputDir, runIndex, 0,  Double.MAX_VALUE, Integer.MAX_VALUE);
            System.out.println("*** EVAL COUNT = " + individualEvaluator.getFunctionEvaluationsCount());
            totalEvaluationsCount += individualEvaluator.getFunctionEvaluationsCount();
            // Reset the number of function evaluations to start
            // counting from Zero again in the next iteration
            individualEvaluator.resetFunctionEvaluationsCount();
        }
        System.out.format("Total Evaluations Count = %d%n", totalEvaluationsCount);
        System.out.format("Avg Evaluations Count(per run) = %d%n", (totalEvaluationsCount / runsCount));

    }
}
