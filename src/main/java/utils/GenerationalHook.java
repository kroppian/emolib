package utils;

import emo.Individual;

import java.io.IOException;

public abstract class GenerationalHook {

    public abstract void run(Individual[] population) throws IOException;

}
